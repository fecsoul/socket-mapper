
class Sockets{
    constructor(io){
        this.io = io;
        this.socketEvents();
       
    }
    socketEvents(){
        //On connection
        this.io.on('connection',(socket)=>{
            console.log('Cliente conectado')

            socket.on('get-geoposition', ( data ) => {
                this.io.emit('send-geoposition', data );
                console.log(data)

            });

            socket.on('get-ip', ( data ) => {
                this.io.emit('send-ip', data );
                console.log(data)

            });

            socket.on('get-nombre-usuario', ( data ) => {
                this.io.emit('send-nombre-usuario', data );
                console.log(data)

            });
            socket.on('get-message', ( data ) => {
                this.io.emit('send-message', data );
                console.log(data)

            });
        })
       
    }
}

module.exports = Sockets;