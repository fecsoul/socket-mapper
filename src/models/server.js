//Servidor de Express
const express  = require('express');
const http     = require('http');
const socketIO = require('socket.io');
const path     = require('path');
const Sockets  = require('./sockets');
const cors     = require('cors');
class Server {
    constructor() {
        this.app = express();
        this.port = 3000;
        this.server = http.createServer(this.app);
        this.io = socketIO(this.server, { 
            cors: { 
                origin: ["https://client-mapper-ikant.onrender.com", 
                "http://127.0.0.1:*"],
                methods: ["GET", "POST"], 
            }, 
        });       
     }

    middlewares() {
        this.app.use(express.static(path.resolve(__dirname, '../public')));
        
        this.app.use(cors({
            origin: ['http://127.0.0.1:*',
             'https://client-mapper-ikant.onrender.com'
           ],
            methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
            credentials: true
        }));   
    
    }
        configurarSockets(){
            new Sockets(this.io);
        }
        execute(){
            this.middlewares();
            this.configurarSockets();
            this.server.listen(this.port, () => {
                console.log('Server abierto en puerto, ', this.port)
            });
    }
}

module.exports = Server;